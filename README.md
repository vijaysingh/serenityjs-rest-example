The Report: [Serenity BDD Report](https://baasie.gitlab.io/serenityjs-rest-example/)

This is an example project of how to use [SerenityJS](http://serenity-js.org/) for rest projects with cucumber.

For now we still need protractor, since cucumber is still in the serenity project.
Also there is a custom serenity_cucumber_adapter.ts so that we don't need protractor to start the project.
The idea is that eventually you don't need these anymore.