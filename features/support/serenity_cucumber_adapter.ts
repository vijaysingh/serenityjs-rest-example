import {scenarioLifeCycleNotifier} from 'serenity-js/lib/serenity-cucumber/cucumber_serenity_notifier';

import {serenityBDDReporter} from '@serenity-js/core/lib/reporting/serenity_bdd_reporter';
import {serenity} from 'serenity-js';

import * as cucumber from 'cucumber';

serenity.configure({
    crew: [
        serenityBDDReporter(),
    ],
});

export = function() {
    this.registerListener(scenarioLifeCycleNotifier());
    this.registerListener(Object.assign(cucumber.Listener(), {
        handleAfterFeaturesEvent: (features: any, callback: () => void) => {
            serenity.stageManager().waitForNextCue().then(() => {
                callback();
            });
        },
    }));
};
