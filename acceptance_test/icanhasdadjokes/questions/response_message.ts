import {Question} from 'serenity-js/lib/screenplay-protractor';
import {CallAnApi} from '../../support/call_an_api';
import {expect} from '../../support/expect';

export const ResponseMessage = () => Question.where(`The Response Message`, actor =>
    CallAnApi.as(actor).getResponse(),
);

export const hasDadJoke = () => fundResponse => {
    return fundResponse.then(message =>
        expect(message.joke).to.not.be.empty,
    );
};

export const hasDadJokeWithId = (id: string) => fundResponse => {
    return fundResponse.then(message =>
        expect(message.id).to.equal(id),
    );
};
