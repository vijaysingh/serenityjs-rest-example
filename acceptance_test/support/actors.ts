import {Actor, Cast} from 'serenity-js/lib/screenplay-protractor';
import {Properties} from '../properties';
import {CallAnApi} from './call_an_api';

export class Actors implements Cast {

    actor(name: string): Actor {
        return Actor.named(name).whoCan(CallAnApi.usingEndpoint(Properties.endPoint));
    }
}
