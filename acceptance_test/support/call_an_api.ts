import {Ability, UsesAbilities} from '@serenity-js/core/lib/screenplay';
import request = require('request-promise');

export class CallAnApi implements Ability {

    private response: PromiseLike<any>;

    /**
     *
     * Instantiates the Ability to CallAnApi, allowing the Actor to interact with a Rest api
     *
     * @param url
     * @return {CallAnApi}
     */
    static usingEndpoint(url: string) {
        return new CallAnApi(url);
    }

    /**
     * Used to access the Actor's ability to CallAnApi from within the Interaction classes, such as GET or PUT
     *
     * @param actor
     * @return {CallAnApi}
     */
    static as(actor: UsesAbilities): CallAnApi {
        return actor.abilityTo(CallAnApi);
    }

    /**
     *
     *
     * @param {String} method
     * @param {string} uri
     * @param {boolean} json
     * @returns {PromiseLike<void>}
     */
    call(method: string, uri: string, json: boolean): PromiseLike<void> {
        const url = this.apiUrl.concat(uri);
        const options = {
            method,
            uri: url,
            json,
        };
        return this.response = request(options).promise();
    }

    getResponse(): PromiseLike<any> {
        return this.response;
    }

    constructor(private apiUrl: string) {

    }

}
